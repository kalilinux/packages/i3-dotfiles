i3-dotfiles (20231026-0kali1) kali-dev; urgency=medium

  * New upstream version 20231026

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 17 Nov 2023 16:06:30 +0700

i3-dotfiles (20230801-0kali1) kali-dev; urgency=medium

  * New upstream version 20230801

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 08 Aug 2023 15:21:24 +0700

i3-dotfiles (20230626-0kali1) kali-dev; urgency=medium

  * Include upstream readme in package
  * New upstream version 20230626

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 28 Jun 2023 11:43:26 +0700

i3-dotfiles (20230426-0kali1) kali-dev; urgency=medium

  * New upstream version 20230426

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 27 Apr 2023 07:55:08 +0700

i3-dotfiles (20230329-0kali1) kali-dev; urgency=medium

  * New upstream version 20230329
  * Update copyright

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 31 Mar 2023 09:48:12 +0700

i3-dotfiles (20230309-0kali2) kali-dev; urgency=medium

  * Fix syntax in copyright (no space)

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 22 Mar 2023 11:36:32 +0700

i3-dotfiles (20230309-0kali1) kali-dev; urgency=medium

  * Include Nerd Fonts
  * New upstream version 20230309
  * Update copyright to drop Iosevka font

 -- Arnaud Rebillout <arnaudr@kali.org>  Wed, 22 Mar 2023 11:31:46 +0700

i3-dotfiles (20230302-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Arnaud Rebillout ]
  * New upstream version 20230302
  * Update copyright
  * Update upstream URL
  * Update README.Debian
  * Rename package from i3-gaps-dotfiles to i3-dotfiles
  * Minor fixes based on lintian messages

 -- Arnaud Rebillout <arnaudr@kali.org>  Thu, 09 Mar 2023 10:42:04 +0700

i3-gaps-dotfiles (20221022-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Sophie Brun ]
  * New upstream version 20221022
  * Add missing dep python3

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 09 Dec 2022 11:56:25 +0100

i3-gaps-dotfiles (20220829-0kali1) kali-dev; urgency=medium

  * New upstream version 20220829

 -- Arnaud Rebillout <arnaudr@kali.org>  Tue, 13 Sep 2022 15:56:23 +0700

i3-gaps-dotfiles (20220814-0kali1) kali-dev; urgency=medium

  * Update standards version and debhelper compat
  * New upstream version 20220814

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 15 Aug 2022 11:29:57 +0700

i3-gaps-dotfiles (20220808-0kali1) kali-experimental; urgency=medium

  * Add a watch file to track new upstream git repo
  * Set the source format to quilt
  * New upstream version 20220808
  * Update upstream url and copyright
  * Install everything in the dotfiles directory

 -- Arnaud Rebillout <arnaudr@kali.org>  Mon, 08 Aug 2022 17:19:35 +0200

i3-gaps-dotfiles (20220303) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Joseph O'Gorman ]
  * Change source to native
  * Remove upstream metadata to move to native
  * Update standards version
  * Update copyright to new filepath
  * Merge branch 'Arszilla/i3-gaps-dotfiles-kali/master' into
    'kali/master'

  [ Arszilla ]
  * Remove unnecessary files
  * Update rofi's config for 1.7.0+ and update paths
  * Update copyright

  [ Ben Wilson ]
  * Remove template comment and switch spaces to tabs
  * Configure git-buildpackage for Kali
  * Add GitLab's CI configuration file

  [ Raphaël Hertzog ]
  * Merge branch 'Arszilla-kali/master-patch-71961' into 'kali/master'

 -- Joseph O'Gorman <gamb1t@kali.org>  Thu, 03 Mar 2022 18:09:38 -0500

i3-gaps-dotfiles (20200720-0kali1) kali-dev; urgency=medium

  * Initial release
  * Rename i3-gaps-dotfiles-docs.docs to i3-gaps-dotfiles.docs
  * Fix file name to call README.debian

 -- Joseph O'Gorman <gamb1t@kali.org>  Mon, 20 Jul 2020 02:02:15 -0400
